var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Place = new Schema({
    location : {
        type: {
            type: String,
            default: 'Point'
        },
        coordinates: [Number]
    },
    name : String
});

Place.index({ location : '2dsphere' });

module.exports = mongoose.model('Place', Place);
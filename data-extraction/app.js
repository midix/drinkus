var mongoose = require('mongoose');


var url = 'mongodb://localhost/passport_local_mongoose_express4';
mongoose.connect(url);
var Place = require('./places');
var barsFrance = require('./bars-france.js');
var Account = require('./account');
var Drink = require('./drink');
var i = 0;

var insertDocument = function(){
  var element = barsFrance[i];
  var place = new Place({location : {type : "Point", coordinates : element.geometry.coordinates}, name : element.properties.name });
  place.save(function(err) {
    if (err) {
      console.log("error : " + err)
    }
    console.log("place inserted: " + place);
    if(i == barsFrance.length - 1){
      console.log("finished");
      countDocuments();
    }
    else {
      i++;
      insertDocument();
    }
  });
}

var insertDocuments = function(){
  console.log("insert");
  Place.remove({}, function(err){
    if(err)console.log(err);
    console.log("dropped");
    insertDocument();
  });

}

var countDocuments = function(){
  Place.find({}, function(err, places) {
    console.log(places.length);
  });
}

var findLocation = function(longitude, latitude, distance) {

  // get the max distance or set it to 8 kilometers
  var maxDistance = distance;

  // we need to convert the distance to radians
  // the raduis of Earth is approximately 6371 kilometers
  //maxDistance /= 6371;

  // get coordinates [ <longitude> , <latitude> ]
  var coords = [];
  coords[0] = longitude;
  coords[1] = latitude;

  // find a location
  Place.find({
    location: {
      $near: {
        $geometry : {
          type : "Point",
          coordinates : coords
        },
        $maxDistance : maxDistance
      }
    }
  }).exec(function(err, locations) {
    if(err) console.log(err);
    console.log(locations.length);
  });
}

var findAccount = function(username){
  Account.find({username : username}).exec(function(err, account) {
    if(err) console.log(err);
    console.log(account);
  });
}

//countDocuments();
//insertDocuments();
//findLocation(2.3522220, 48.8566140, 2000000);
//findLocation();

Drink.find({}, function(err, drinks){
  for(var i = 0; i < drinks.length; i++){
    console.log(drinks[i]);
  }

});
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Account = new Schema({
    username: String,
    password: String,
    email: String,
    gender: String,
    town: String,
    birthday: String
});

Account.methods.validPassword = function(pwd){
    return this.password === pwd;
}


module.exports = mongoose.model('accounts', Account);
var express = require('express');
var http = require('http');
var datas = require('../bars-france.js');
var fs = require('fs');
var router = express.Router();

var jsonResult = [];
var i = 0;
var nbFound = 0;
/* GET home page. */
router.get('/', function(req, res, next) {
  console.log(datas.length);

  callWebService(i, res);
});

function callWebService(res){
  console.log(i);
  var lat = datas[i].lat;
  var long = datas[i].lon;
  var path = '/countryCodeJSON?lat=' + lat + '&lng=' + long + '&username=demo';
  //console.log("path : " + path);
  http.get({
    host: 'api.geonames.org',
    path: path
  }, function (response) {
    //console.log("response");
    var str = '';

    //another chunk of data has been recieved, so append it to `str`
    response.on('data', function (chunk) {
      //console.log("data : " + chunk);
      str += chunk;
    });

    //the whole response has been recieved, so we just print it out here
    response.on('end', function () {
      var json = JSON.parse(str);
      //console.log(json.countryName);
      if(json.countryName == "France") {
        jsonResult.push(datas[i]);
        nbFound++;
        console.log(i + " inserted / found : " + nbFound);
        console.log(jsonResult.length);
      }
      //console.log("ready for the next");
      if(i == (datas.length - 1)){
        console.log("nb results : " + jsonResult.length);
        fs.writeFile("/tmp/result.json", JSON.stringify(jsonResult), function(err) {
          if(err) {
            return console.log(err);
          }

          console.log("The file was saved!");
          res.render({result:"OK"});
        });
      }
      else {
        i++;
        callWebService(res);
      }
    });

    response.on('error', function(err){
      console.log("Error Occurred: "+err.message);
    });
  });
}

module.exports = router;

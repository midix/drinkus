var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Drink = new Schema({
    creator:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'accounts'
    },
    joins:[{
        userId:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'accounts'
        }
    }],
    place:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Place'
    },
    nbGirls: Number,
    nbGuys: Number,
    date: Date,
    minAge: { type: Number, default: 0},
    maxAge: { type: Number, default: 100},
    interests: []
});

module.exports = mongoose.model('Drink', Drink);